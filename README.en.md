# MySQL

[eu](README.md) | [es](README.es.md) | [en](README.en.md)

## Whithout docker

* Enter to your MySQL server (with an editor or command line tool).
* Create ```mvc_exercise``` database.

```sql
CREATE DATABASE mvc_exercise;
```

* Create user for the exercises:

```sql
CREATE USER 'admin'@'%'
  IDENTIFIED BY 'admin@eskola';
GRANT ALL
  ON mvc_exercise.*
  TO 'admin'@'%';
USE mvc_exercise;
```

* Take [```sql-files/mvc_exercise.sql```](./sql-files/mvc_exercise.sql) file content, paste it and execute it.

## With Docker

We will mount 2 volumes:

  1. ```sql-files/``` folder:  when creating the container, ```mvc_exercise.sql``` file will be taken into account, so tables and content within it will be created.
  1. ```./volume``` folder will store the database itself, so if we remove the container, all the changes we made will be save. If we want to start from the beginning, we will have to remove the container and ```./volume``` folder.

* To make it work **without docker compose** for the first time:

```bash
docker run -d \
  --name mysql_web \
  -p 3306:3306 \
  -e MYSQL_ROOT_PASSWORD=admin@eskola \
  -e MYSQL_DATABASE=mvc_exercise \
  -e MYSQL_USER=admin \
  -e MYSQL_PASSWORD=admin@eskola \
  -v $(pwd)/sql-files/:/docker-entrypoint-initdb.d/ \
  -v $(pwd)/volume:/var/lib/mysql \
  mysql:8
```

* To start/stop it afterwards:

```bash
docker start mysql_web
```

```bash
docker stop mysql_web
```

* To make it work using **Docker Compose**:

```bash
docker-compose up -d
```

* Or

```bash
docker compose up -d
```

* To stop it:

```bash
docker-compose down
```

* Or

```bash
docker compose down
```