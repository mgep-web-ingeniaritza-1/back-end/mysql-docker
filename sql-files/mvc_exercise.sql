USE mvc_exercise;

DROP TABLE IF EXISTS `news_item`;

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
    `userId` int(11) NOT NULL AUTO_INCREMENT,
    `username` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
    `password` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
    `first_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
    `second_name` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
    `email` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
    PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

INSERT INTO `user` VALUES (1,'admin','admin@eskola','Admin','Administrator','admin@example.com');

CREATE TABLE `news_item` (
    `newsItemId` int(11) NOT NULL AUTO_INCREMENT,
    `title` text NOT NULL,
    `body` text NOT NULL,
    `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
    `lang` varchar(45) DEFAULT NULL,
    `authorId` int(11) NOT NULL,
    PRIMARY KEY (`newsItemId`),
    KEY `userId_idx` (`authorId`),
    CONSTRAINT `userId` FOREIGN KEY (`authorId`) REFERENCES `user` (`userId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

INSERT INTO `news_item`
VALUES
(1,'News Item 1','<p>This is the main content of the first news item</p>','2016-10-26 13:31:16','en',1),
(2,'1go Berria','<p>Hau euskaraz idatzitako lehenengo berria da</p>','2016-10-26 14:04:56','eu',1),
(3,'News Item 2','<p>\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean vehicula ullamcorper odio ut eleifend. Maecenas finibus, quam nec mollis venenatis, diam felis congue lorem, vitae ullamcorper risus ante eu dui. Duis vitae feugiat orci. Praesent hendrerit lorem vitae facilisis gravida. In congue blandit semper. Proin sit amet ante et diam faucibus finibus. Sed sit amet turpis leo. Fusce sed orci hendrerit erat blandit bibendum.\n</p>\n<p>\nDuis sed mattis lorem, ac consequat elit. Donec vitae vulputate ante. Pellentesque sollicitudin ex nec convallis mollis. Integer at lacus aliquet, congue libero at, accumsan lorem. Ut non libero tincidunt, euismod nulla ut, mollis metus. Nulla dignissim nisl eu nisl porta fringilla. Vestibulum venenatis, lacus eget laoreet elementum, magna dui sollicitudin felis, cursus ultrices velit erat eu tortor. Quisque pharetra aliquet eros, at mattis lorem ornare sed. In rhoncus ligula ac mattis dictum. Sed non arcu eu est aliquet consectetur. Donec egestas eget sapien ac ultricies. In quis tristique metus, nec dictum sem. Integer id lacus quis sem consequat semper. Nullam tempus lacus ac arcu sodales, et iaculis quam hendrerit. Nullam porttitor quam id interdum vehicula. Suspendisse quis varius nisl.\n</p>\n<p>\nVivamus vel convallis mi. Ut rutrum varius velit in varius. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Proin interdum ex eget risus scelerisque elementum. Vivamus imperdiet lorem ut est pulvinar dictum. Quisque aliquet risus ac justo ultrices, at vehicula eros accumsan. Ut rutrum magna congue nibh suscipit, nec consequat elit fermentum. Sed tempus pharetra diam, eget convallis leo posuere ut. Suspendisse eget posuere nisi. Nulla fringilla massa ac neque laoreet, sit amet interdum est tristique. Praesent pretium ipsum et ligula pulvinar, sit amet porttitor eros porttitor. Nunc pharetra turpis in lectus mollis, quis posuere risus sagittis. Suspendisse potenti. Praesent mollis neque ut ex mollis euismod ac id erat.\n</p>\n<p>\nAenean ac ligula libero. Morbi vel finibus ex. Proin a orci imperdiet, lacinia urna eget, sollicitudin diam. Mauris vehicula purus at nulla sagittis blandit. Vivamus fringilla enim id ante tincidunt elementum. Pellentesque efficitur erat ac lacus sollicitudin iaculis. Vestibulum molestie, dolor consectetur pharetra porttitor, massa erat tempus tellus, vitae tempus libero velit tempus erat. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed sit amet sagittis nisl. Proin eget nisl neque. Nulla sodales, orci vitae pellentesque viverra, erat massa laoreet tellus, sit amet semper est mauris vel metus. Nunc lacinia mattis tempor. Praesent mi dui, varius ut maximus vel, accumsan dapibus magna. Curabitur ut facilisis felis. Duis quam nisl, condimentum sit amet lacinia eget, iaculis et magna. Vestibulum suscipit pellentesque consequat.\n</p>\n<p>\nPellentesque scelerisque nibh eget nunc egestas, eget sodales odio scelerisque. Mauris fermentum porta justo, sit amet aliquet nulla dictum non. Nullam posuere purus ut turpis fringilla tempor. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam at elementum diam. Phasellus eget sollicitudin urna. Nulla in consequat enim, nec facilisis ligula. Cras lobortis diam non justo condimentum, eget cursus metus luctus. Nam euismod eleifend sodales. Sed sodales mauris id ante consectetur auctor.\n</p>','2016-10-26 14:08:32','en',1);
