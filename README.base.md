<!---------------------------->
<!-- multilingual suffix: en, es, eu -->
<!-- no suffix: eu -->
<!---------------------------->
<!-- [common] -->
# MySQL

[eu](README.md) | [es](README.es.md) | [en](README.en.md)

<!-- [en] -->
## Whithout docker

* Enter to your MySQL server (with an editor or command line tool).
* Create ```mvc_exercise``` database.

<!-- [eu] -->
## Docker gabe

* Sartu zure MySQL zerbitzarian (editore batekin edo terminaleko aplikaziotik).
* Sortu ```mvc_exercise``` datu basea.

<!-- [es] -->
## Sin Docker

* Entra en tu servidor MySQL (con un editor o línea de comandos).
* Crea la base de datos ```mvc_exercise```.

<!-- [common] -->
```sql
CREATE DATABASE mvc_exercise;
```

<!-- [en] -->
* Create user for the exercises:

<!-- [eu] -->
* Sortu ariketetan erabiliko dugun erabiltzeilea:

<!-- [es] -->
* Crea el usuario que utilizaremos en los ejercicios:

<!-- [common] -->
```sql
CREATE USER 'admin'@'%'
  IDENTIFIED BY 'admin@eskola';
GRANT ALL
  ON mvc_exercise.*
  TO 'admin'@'%';
USE mvc_exercise;
```

<!-- [en] -->
* Take [```sql-files/mvc_exercise.sql```](./sql-files/mvc_exercise.sql) file content, paste it and execute it.

<!-- [eu] -->
* [```sql-files/mvc_exercise.sql```](./sql-files/mvc_exercise.sql) fitxategiko edukia kopiatu, itsatsi eta exekutatu.

<!-- [es] -->
* Copia el contenido del fichero [```sql-files/mvc_exercise.sql```](./sql-files/mvc_exercise.sql), pégalo y ejecútalo.

<!-- [en] -->
## With Docker

We will mount 2 volumes:

  1. ```sql-files/``` folder:  when creating the container, ```mvc_exercise.sql``` file will be taken into account, so tables and content within it will be created.
  1. ```./volume``` folder will store the database itself, so if we remove the container, all the changes we made will be save. If we want to start from the beginning, we will have to remove the container and ```./volume``` folder.

* To make it work **without docker compose** for the first time:

<!-- [eu] -->
## Docker erabiliz

* 2 bolumen montatuko ditugu:

  1. ```sql-files/``` karpeta: datu basea sortzerako orduan ```mvc_exercise.sql``` fitxategia hartuko du kontutan eta bertako taulak eta edukia sartuko ditu.
  1. ```./volume``` karpetan datu basea bera montatuko dugu, aldaketak gorde daitezen kontenedorea ezabatzen badugu ere. 0tik hasi nahi badugu, ```./volume``` carpeta eta kontenedoea ezabatu.

* **Docker compose gabe** martxan jartzeko lehenengo aldiz:

<!-- [es] -->
## Con Docker

* Montaremos 2 volúmenes:

  1. La carpeta ```sql-files/```: al crear la base de datos, se tomará en cuenta el fichero ```mvc_exercise.sql``` y se crearán las tablas y contenido de dicho fichero.
  1. La carpeta ```./volume```: guardará la base de datos en si, para guardar los datos aunque borremos el contenedor.Para empezar de 0, hay que borran el contenedor y la carpeta ```./volume```.

* Para ponerlo en marcha **sin docker compose** por primera vez:

<!-- [common] -->
```bash
docker run -d \
  --name mysql_web \
  -p 3306:3306 \
  -e MYSQL_ROOT_PASSWORD=admin@eskola \
  -e MYSQL_DATABASE=mvc_exercise \
  -e MYSQL_USER=admin \
  -e MYSQL_PASSWORD=admin@eskola \
  -v $(pwd)/sql-files/:/docker-entrypoint-initdb.d/ \
  -v $(pwd)/volume:/var/lib/mysql \
  mysql:8
```

<!-- [en] -->
* To start/stop it afterwards:

<!-- [eu] -->
* Martxan jartzeko edo geratzeko hortik aurrera:

<!-- [es] -->
* Para iniciarlo o pararle las siguientes veces:

<!-- [common] -->
```bash
docker start mysql_web
```

```bash
docker stop mysql_web
```

<!-- [en] -->
* To make it work using **Docker Compose**:

<!-- [eu] -->
* Martxan jartzeko **Docker Compose** erabiliz:

<!-- [es] -->
* Para ponerlo en marcha con **Docker Compose**:

<!-- [common] -->
```bash
docker-compose up -d
```

<!-- [en] -->
* Or

<!-- [eu] -->
* Edo:

<!-- [es] -->
* O:

<!-- [common] -->
```bash
docker compose up -d
```

<!-- [en] -->
* To stop it:

<!-- [eu] -->
* Gelditzeko:

<!-- [es] -->
* Para pararlo:

<!-- [common] -->
```bash
docker-compose down
```

<!-- [en] -->
* Or

<!-- [eu] -->
* Edo:

<!-- [es] -->
* O:

<!-- [common] -->
```bash
docker compose down
```