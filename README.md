# MySQL

[eu](README.md) | [es](README.es.md) | [en](README.en.md)

## Docker gabe

* Sartu zure MySQL zerbitzarian (editore batekin edo terminaleko aplikaziotik).
* Sortu ```mvc_exercise``` datu basea.

```sql
CREATE DATABASE mvc_exercise;
```

* Sortu ariketetan erabiliko dugun erabiltzeilea:

```sql
CREATE USER 'admin'@'%'
  IDENTIFIED BY 'admin@eskola';
GRANT ALL
  ON mvc_exercise.*
  TO 'admin'@'%';
USE mvc_exercise;
```

* [```sql-files/mvc_exercise.sql```](./sql-files/mvc_exercise.sql) fitxategiko edukia kopiatu, itsatsi eta exekutatu.

## Docker erabiliz

* 2 bolumen montatuko ditugu:

  1. ```sql-files/``` karpeta: datu basea sortzerako orduan ```mvc_exercise.sql``` fitxategia hartuko du kontutan eta bertako taulak eta edukia sartuko ditu.
  1. ```./volume``` karpetan datu basea bera montatuko dugu, aldaketak gorde daitezen kontenedorea ezabatzen badugu ere. 0tik hasi nahi badugu, ```./volume``` carpeta eta kontenedoea ezabatu.

* **Docker compose gabe** martxan jartzeko lehenengo aldiz:

```bash
docker run -d \
  --name mysql_web \
  -p 3306:3306 \
  -e MYSQL_ROOT_PASSWORD=admin@eskola \
  -e MYSQL_DATABASE=mvc_exercise \
  -e MYSQL_USER=admin \
  -e MYSQL_PASSWORD=admin@eskola \
  -v $(pwd)/sql-files/:/docker-entrypoint-initdb.d/ \
  -v $(pwd)/volume:/var/lib/mysql \
  mysql:8
```

* Martxan jartzeko edo geratzeko hortik aurrera:

```bash
docker start mysql_web
```

```bash
docker stop mysql_web
```

* Martxan jartzeko **Docker Compose** erabiliz:

```bash
docker-compose up -d
```

* Edo:

```bash
docker compose up -d
```

* Gelditzeko:

```bash
docker-compose down
```

* Edo:

```bash
docker compose down
```