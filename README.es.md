# MySQL

[eu](README.md) | [es](README.es.md) | [en](README.en.md)

## Sin Docker

* Entra en tu servidor MySQL (con un editor o línea de comandos).
* Crea la base de datos ```mvc_exercise```.

```sql
CREATE DATABASE mvc_exercise;
```

* Crea el usuario que utilizaremos en los ejercicios:

```sql
CREATE USER 'admin'@'%'
  IDENTIFIED BY 'admin@eskola';
GRANT ALL
  ON mvc_exercise.*
  TO 'admin'@'%';
USE mvc_exercise;
```

* Copia el contenido del fichero [```sql-files/mvc_exercise.sql```](./sql-files/mvc_exercise.sql), pégalo y ejecútalo.

## Con Docker

* Montaremos 2 volúmenes:

  1. La carpeta ```sql-files/```: al crear la base de datos, se tomará en cuenta el fichero ```mvc_exercise.sql``` y se crearán las tablas y contenido de dicho fichero.
  1. La carpeta ```./volume```: guardará la base de datos en si, para guardar los datos aunque borremos el contenedor.Para empezar de 0, hay que borran el contenedor y la carpeta ```./volume```.

* Para ponerlo en marcha **sin docker compose** por primera vez:

```bash
docker run -d \
  --name mysql_web \
  -p 3306:3306 \
  -e MYSQL_ROOT_PASSWORD=admin@eskola \
  -e MYSQL_DATABASE=mvc_exercise \
  -e MYSQL_USER=admin \
  -e MYSQL_PASSWORD=admin@eskola \
  -v $(pwd)/sql-files/:/docker-entrypoint-initdb.d/ \
  -v $(pwd)/volume:/var/lib/mysql \
  mysql:8
```

* Para iniciarlo o pararle las siguientes veces:

```bash
docker start mysql_web
```

```bash
docker stop mysql_web
```

* Para ponerlo en marcha con **Docker Compose**:

```bash
docker-compose up -d
```

* O:

```bash
docker compose up -d
```

* Para pararlo:

```bash
docker-compose down
```

* O:

```bash
docker compose down
```